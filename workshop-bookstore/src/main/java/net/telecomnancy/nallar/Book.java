package net.telecomnancy.nallar;

import net.tncy.validator.constraints.books.ISBN;

public class Book {
    private String title;
    private String author;
    @ISBN
    private String isbn;

    public Book(String title, String author, String isbn) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }

    public Book() {
        this.title="Titre";
        this.author="Moi";
        this.isbn="AHBDHJEBJDEBKJDNBQKJ";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}

