package net.telecomnancy.nallar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void test1(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Book b = new Book();
        b.setIsbn("1-4028-9462-7");
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(b);
        assertEquals(0,constraintViolations.size());
    }

    @Test
    public void test2(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Book b = new Book();
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(b);
        assertEquals(1,constraintViolations.size());
    }
}
